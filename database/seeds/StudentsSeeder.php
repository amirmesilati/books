<?php

use Illuminate\Database\Seeder;

class StudentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    
                    'name' => 'chilki',
                    'email' => 'chilki@gmail.com',
                    'password' => Hash::make('123456789'),
                    'created_at' => date('Y-m-d G:i:s'),
                    'role' => 'student',
                ],
                [
                    
                    'name' => 'tinki',
                    'email' => 'tinki@gmail.com',
                    'password' =>  Hash::make('123456789'),
                    'created_at' => date('Y-m-d G:i:s'),
                    'role' => 'student',
                ],
                [
                    
                    'name' => 'bilki',
                    'email' => 'bilki@gmail.com',
                    'password' => Hash::make('123456789'),
                    'created_at' => date('Y-m-d G:i:s'),
                    'role' => 'student',
                ],
            ]);
    }
}
