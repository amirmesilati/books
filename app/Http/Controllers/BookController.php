<?php

namespace App\Http\Controllers;
use App\Book;
use App\User;   
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\BookValidationRequest;
class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all todos
       // $id = 1; //or later change to 2
       // $id=Auth::id();
       $id = Auth::id();
       if (Gate::denies('teacher')) {
           $boss = DB::table('students')->where('student',$id)->first();
           $id = $boss->teacher;
       }
       $user = User::find($id);
       $books = $user->books;

        return view('books.index', ['books'=>$books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('teacher')) {
            abort(403,"Sorry you are not allowed to create books..");
        }
 
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookValidationRequest $request)
    {
        if (Gate::denies('teacher')) {
            abort(403,"Are you a hacker or what?");
       
        }
        
        $validated = $request->validated();

        $book = new Book();
      //$id = 1;
      $id=Auth::id();
      $book->title = $request->title;
      $book->author = $request->author;
      $book->status = 0; 
      $book->user_id = $id;
      $book->save();
      return redirect('books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('teacher')) {
            abort(403,"Are you a hacker or what?");
       
        }
        $book = Book::find($id);
        return view('books.edit', ['book' => $book]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function update(Request $request, $id)
    {
     //   $book = Book::find($id);
     //   if($book->status == 0){ // אי אפשר למחוק צקבוקס שסומן
     //   if(!$book->user->id == Auth::id()) return(redirect('books'));
     //       $book->update($request ->except(['_token']));
     //       if($request->ajax()){
     //        return Response::json(array('result'=>'success','status'=>$request->status),200);    
     //      }
     //   }
        //  $book = Book::find($id);
      //  $book->update($request -> all());
     //   return redirect('books');
            //only if this todo belongs to user
            $book = Book::findOrFail($id);
            //employees are not allowed to change the title 
             if (Gate::denies('teacher')) {
                 if ($request->has('update'))
                        abort(403,"You are not allowed to edit todos..");
             }   
             if(Gate::allows('teacher')){
             //make sure the todo belongs to the logged in user
             if(!$book->user->id == Auth::id()) return(redirect('books'));
             $book->update($request->except(['_token']));
              if($request->ajax()){
                   return Response::json(array('result' => 'success1','status' => $request->status ), 200);                      
                 }
             }
             if(Gate::allows('student')){
                if($book->status == 0){
                if(!$book->user->id == Auth::id()) return(redirect('books'));
                  $book->update($request->except(['_token']));
                    if($request->ajax()){
                         return Response::json(array('result' => 'success1','status' => $request->status ), 200);                      
                       }
                  }
                }
             return redirect('books');
            
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('teacher')) {
            abort(403,"Are you a hacker or what?");
       
        }
        $book = Book::find($id);
        $book->delete(); 
        return redirect('books');
    }
}
