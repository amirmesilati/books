@extends('layouts.app')
@section('content')

<h1>This is your books list</h1>

<table border="3">
<thead>  
     <tr>
     <th>reading</th>  
     <th>title</th> 
     <th>author</th> 
     <th>chenage</th> 
     </tr>  
 </thead>
    
    @foreach($books as $book)
        <tr>
      
            <td>
            @if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$book->id}}">
       @endif
                    </td>
            <td>{{$book->title }}</td>
            <td>{{$book->author }}</td>
            <td><a href ="{{route('books.edit', $book ->id)}}" >update</a></td>
        </tr>
            @endforeach
</table>
@cannot('student')  <a href ="{{route('books.create')}}"> create a new book </a>@endcannot

<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
            console.log(event.target.id)
               $.ajax({                  
                   url:  "{{url('books')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               
               });  
                         
           });
       });
   </script> 
@endsection